<?php

return [

    /**
     * The DocuSign Integrator's Key
     */

    'integrator_key' => '96fd4cd3-081c-4dd2-a567-f36bab6f92ae',

    /**
     * The Docusign Account Email
     */
    'email' => 'ammarzai@gmail.com',

    /**
     * The Docusign Account Password
     */
    'password' => '12345678',

    /**
     * The version of DocuSign API (Ex: v1, v2)
     */
    'version' => 'v2',

    /**
     * The DocuSign Environment (Ex: demo, test, www)
     */
    'environment' => 'demo',

    /**
     * The DocuSign Account Id
     */
    'account_id' => '53ff83af-0616-4807-b175-62678d36a300',
    
    /**
     * Envelope ID field (for Envelope trait) 
     */
    'envelope_field' => 'envelopeId',
];
