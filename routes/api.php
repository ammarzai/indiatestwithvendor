<?php
header('Access-Control-Allow-Origin: *');
header( 'Access-Control-Allow-Headers: Authorization, Content-Type' );
use Illuminate\Http\Request;



/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('auth/register', 'Auth\ApiRegisterController@store');
//Route::post('auth/login', 'Auth\ApiLoginController@store');
//Route::get('auth/product', 'Auth\ApiProductController@index')->middleware('cors');

//Route::match(['post', 'options'], 'api/...', 'Api\XController@method')->middleware('cors');

/*Route::prefix('v1')->namespace('API')->group(function () {
  Route::get('/product', 'ApiProductController@index')->middleware('cors');
  // Login
 // Route::post('/login','AuthController@postLogin');
  // Register
  Route::post('/register','AuthController@postRegister');
  
  // Protected with APIToken Middleware
  Route::middleware('APIToken')->group(function () {
     Route::post('/cart','AuthController@postCart');
    // Logout
    Route::post('/logout','AuthController@postLogout');
  });
});*/



Route::group(['prefix' => 'v1','namespace' => 'API'], function () {

    

    Route::post('login', 'AuthController@postLogin');
     Route::post('/register','AuthController@postRegister');
      Route::get('/product', 'ApiProductController@index');
    Route::middleware('APIToken')->group(function () {
     Route::post('/cart','AuthController@postCart');
    // Logout
    Route::post('/logout','AuthController@postLogout');
  });

});
