<?php

namespace Campaign\Response;

use Illuminate\Support\ServiceProvider;

class CampaignServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
     protected $defer = true;
    public function boot()
    {
        //
        include __DIR__.'/routes.php';
        $this->app->make('Campaign\Response\CampaignController');
       
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
