<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OrderDetail extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_id',
        'order_number_detail',
        'product_id',
        'quantity',
        'price',
        'sub_total',
    ];
     public function orderdetailproduct()
    {
        return $this->hasOne('App\Models\Product','id','product_id');
    }
}
