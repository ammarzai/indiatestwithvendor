<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Order extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'order_number',
        'transaction_date',
        'customer_id',
        'total_amount',
        'status',
    ];
     public function customer()
    {
        return $this->hasOne('App\Models\Customer','id','customer_id');
    }
     public function orderdetail()
    {
        return $this->hasMany('App\Models\OrderDetail','order_id','id');
    }
}
