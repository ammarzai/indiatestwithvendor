<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAutoresponder extends Model
{
    //
    protected $table="user_autoresponder";
    public function auto_detail() {
        return $this->hasOne('App\AutoresponderDetails','user_autoresponder_id');
    }
}
