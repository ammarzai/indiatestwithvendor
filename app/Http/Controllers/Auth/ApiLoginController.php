<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Http\Controllers\Controller;
//use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Auth\LoginController;
class ApiLoginController extends Controller
{
    /**
     * Handle a registration request for the application.
     *
     * @override
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
   public function login(Request $request){

    if (Auth::check(['email' => $request->email, 'password' => $request->password])) {
        //$student = Student::where('email',$request->email)->first();
        $student = Auth::user();
        $student->api_token = str_random(60);
        $student->save();
        return response([
            'status' => Response::HTTP_OK,
            'response_time' => microtime(true) - LARAVEL_START,
            'student' => $student
        ],Response::HTTP_OK);
    }

    return response([
        'status' => Response::HTTP_BAD_REQUEST,
        'response_time' => microtime(true) - LARAVEL_START,
        'error' => 'Wrong email or password',
        'request' => $request->all()
    ],Response::HTTP_BAD_REQUEST);

}
}
