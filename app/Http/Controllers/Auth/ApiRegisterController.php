<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Models\Customer;
use App\Http\Controllers\Controller;
//use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Auth\RegisterController;
class ApiRegisterController extends Controller
{
    /**
     * Handle a registration request for the application.
     *
     * @override
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
         $customer = Customer::create([
            'first_name' => $request->input('firstname'),
            'last_name' => $request->input('lastname'),
            'email' => $request->input('email'),
            'postal_address' => $request->input('postal_address'),
            'physical_address' => $request->input('physical_add'),
            'password' => bcrypt($request->input('password')),
        ]);
         $response_array['code']     = '1';
         return response()->json($response_array, 200);

    }
}
