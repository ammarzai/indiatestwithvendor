<?php

namespace App\Http\Controllers\Auth;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Http\Controllers\Controller;
//use Illuminate\Auth\Events\Registered;
use App\Http\Controllers\Auth\RegisterController;
class ApiProductController extends Controller
{
    /**
     * Handle a registration request for the application.
     *
     * @override
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        
          $products = Product::all();

        $params = [
            'title' => 'Products Listing',
            'products' => $products,
        ];
        return response()->json($products, 200);
        //return view('admin.products.products_list')->with($params);
    }
}
