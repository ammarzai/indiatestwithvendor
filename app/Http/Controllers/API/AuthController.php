<?php
namespace App\Http\Controllers\API;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use App\Models\User;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDetail;
use Validator;
class AuthController extends Controller
{
  private $apiToken;
  public function __construct()
  {
    // Unique Token
    $this->apiToken = uniqid(base64_encode(str_random(60)));
  }
  /**
   * Client Login
   */
  public function postLogin(Request $request)
  {
    // Validations

    $rules = [
      /*'email'=>'required|email',
      'password'=>'required'*/
    ];
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
      // Validation failed
      return response()->json([
        'message' => $validator->messages(),
      ]);
    } else {
      // Fetch User
      $user = Customer::where('email',$request->index_signin)->first();

      if($user) {
        // Verify the password
        if( password_verify($request->password_signin, $user->password) ) {
          // Update Token
          $postArray = ['api_token' => $this->apiToken];
          $login = Customer::where('email',$request->index_signin)->update($postArray);
         
          if($login) {
            return response()->json([
              'name'         => $user->name,
              'email'        => $user->email,
              'access_token' => $this->apiToken,
              'login' => '1',
              'message' => 'Login successfully',
            ]);
          }
        } else {
          return response()->json([
            'message' => 'Invalid Password',
            'login' => '0',
          ]);
        }
      } else {
        return response()->json([
          'message' => 'User not found',
        ]);
      }
    }
  }
  /**
   * Register
   */
  public function postRegister(Request $request)
  {
    // Validations
    $rules = [
      
      'email'    => 'required|unique:customers,email',
      'password' => 'required'
    ];
    $validator = Validator::make($request->all(), $rules);
    if ($validator->fails()) {
      // Validation failed
      return response()->json([
        'message' => $validator->messages(),
      ]);
    } else {
      $postArray = [
            'first_name' => $request->input('firstname'),
            'last_name' => $request->input('lastname'),
            'email' => $request->input('email'),
            'postal_address' => $request->input('postal_address'),
            'physical_address' => $request->input('physical_add'),
            'password' => bcrypt($request->input('password')),
            'api_token' => $this->apiToken
        ];
      // $user = User::GetInsertId($postArray);
      $user = Customer::insert($postArray);
  
      if($user) {
      	
        return response()->json([
          'name'         => $request->firstname,
          'email'        => $request->email,
          'access_token' => $this->apiToken,
          'code'		=> '1'
        ],200);
      } else {
        return response()->json([
          'message' => 'Registration failed, please try again.',
        ]);
      }
    }
  }
  /**
   * Logout
   */
  public function postLogout(Request $request)
  {
    $token = $request->header('Authorization');
    $user = User::where('api_token',$token)->first();
    if($user) {
      $postArray = ['api_token' => null];
      $logout = User::where('id',$user->id)->update($postArray);
      if($logout) {
        return response()->json([
          'message' => 'User Logged Out',
        ]);
      }
    } else {
      return response()->json([
        'message' => 'User not found',
      ]);
    }
  }

  public function postCart(Request $request)
  {
  	/*echo "<pre>";
    echo date('Y-m-d H:i:s');
  	print_r($request->all());*/
  //	echo $request->header('api_token');
     $token = $request->header('Authorization');
     $user = Customer::select('id')->where('api_token',$token)->first();
/*echo "<pre>";
print_r($user->id);*/
//$order = new Order;

//echo $order->user_id = Auth()->id();
//$latestOrder = Order::orderBy('created_at','DESC')->first();

/*echo "<pre>";
print_r($request->all());
*/
$order_detail=$request->all();


$today = date("Ymd");
$rand = strtoupper(substr(uniqid(sha1(time())),0,4));
 $unique = $today . $rand;
$sqlInsert = array('order_number'=>$unique, 'transaction_date'=>date('Y-m-d H:i:s'),'customer_id'=>$user->id,'total_amount'=>$request->total,'status'=>'1'
);

$oid=Order::create($sqlInsert);

foreach ($order_detail['data'] as $key => $value) {

$a[$key]['product_id']=$value["sku"];
$a[$key]['quantity']=$value["quantity"];
$a[$key]['price']=$value["unitprice"];
$a[$key]['sub_total']=$value["quantity"]*$value["unitprice"];
$a[$key]['order_id']=$oid->id;
$a[$key]['order_number_detail']=$oid->order_number;
$a[$key]['created_at']=date('Y-m-d H:i:s');
$a[$key]['updated_at']=date('Y-m-d H:i:s');
}
OrderDetail::insert($a);
return response()->json([
          'message' => 'Order placed',
        ]);

//DB::table('table_name')->insert($sqlInsert);


/*if(!empty($latestOrder)){
$order_nr = '#'.str_pad($latestOrder->id + 1, 8, "0", STR_PAD_LEFT);
}else{
  
$order_nr = '#'.str_pad(10 + 1, 8, "0", STR_PAD_LEFT);
}*/



//$order->save();

/*
  	$sqlInsert = array(
    array('id'=>1, 'temp'=>1),
    array('id'=>2, 'temp'=>2),
    array('id'=>3, 'temp'=>3),
);

DB::table('table_name')->insert($sqlInsert);*/

  }
}