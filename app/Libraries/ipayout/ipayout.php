<?php
namespace App\Libraries\ipayout;

use anlutro\cURL\Laravel\cURL;
use Illuminate\Support\Facades\Log;
use Monolog\Logger;

class iPayout
{
    protected $_MerchantName;
    protected $_MerchantGUID;
    protected $_MerchantPassword;
    protected $_eWalletAPIURL;
    protected $_eWalletJsonAPIURL;

    private $_friendlyResponse;
    private $_eWalletResponse;

    public function __construct()
    {
        $mode = env('IPAYOUT_MODE', 'TEST');
        $this->_MerchantName = env('IPAYOUT_MERCHANTNAME');
        $this->_MerchantGUID = env('IPAYOUT_MERCHANTGUID');

        if ( strtoupper($mode) == "TEST" ) {
            $this->_eWalletAPIURL = "https://testewallet.com/eWalletWS/ws_Adapter.aspx";
            $this->_eWalletJsonAPIURL = "https://testewallet.com/eWalletWS/ws_JsonAdapter.aspx";
            $this->_MerchantPassword = env('IPAYOUT_TEST_MERCHANTPASSWORD');
        }
        if ( strtoupper($mode) == "PRODUCTION" ) {
            $this->_eWalletAPIURL = "https://www.i-payout.net/eWalletWS/ws_Adapter.aspx";
            $this->_eWalletJsonAPIURL = "https://www.i-payout.net/eWalletWS/ws_JsonAdapter.aspx";
            $this->_MerchantPassword = env('IPAYOUT_PRODUCTION_MERCHANTPASSWORD');
        }

        // setting some variables ahead of time.
        $this->_friendlyResponse = array();
    }
    public function checkThisUsername($username)
    {
        $info = array(
            "fn"                =>  "eWallet_GetUserAccountStatus",
            "MerchantGUID"      =>  $this->_MerchantGUID,
            "MerchantPassword"  =>  $this->_MerchantPassword,
            "UserName"          =>  $username,
        );

        $this->_eWalletResponse = $this->makeJsonCall($info);

        $status_code = $this->statusCode($this->_eWalletResponse['response']['m_Code']);

        if ( $status_code['text'] == 'CUSTOMER_NOT_FOUND' ) {
            $this->_friendlyResponse = array(
                'customer_exists'       =>  false,
                'msg'                   =>  $this->_eWalletResponse['response']['m_Text'],
                'status'                =>  $status_code,
            );
        }
        if ( $status_code['text'] == 'NO_ERROR' ) {
            $this->_friendlyResponse = array(
                'customer_exists'       =>  true,
                'msg'                   =>  $this->_eWalletResponse['response']['m_Text'],
                'AccStatus'             =>  $this->_eWalletResponse['response']['AccStatus'],
            );
        }

        $response = array(
            'ewallet_response'  =>  $this->_eWalletResponse,
            'class_response'    =>  $this->_friendlyResponse,
        );

        return $response;
    }
    public function registerUser($username, $firstname, $lastname, $email)
    {
        $info = array(
            "fn"					=> "eWallet_RegisterUser",
            "MerchantGUID"			=> $this->_MerchantGUID,
            "MerchantPassword"	    => $this->_MerchantPassword,
            "UserName"				=> $username,
            "FirstName"			    => $firstname,
            "LastName"				=> $lastname,
            "EmailAddress"			=> $email,
            "DateOfBirth"			=> "1/1/1900",
        );

        $this->_eWalletResponse = $this->makeJsonCall($info);

        $this->_friendlyResponse = [];

        $response = $this->returnResponse($this->_eWalletResponse, $this->_friendlyResponse);

        return $response;
    }
    public function RequestUserAutoLogin($username)
    {
        //fn = eWallet_RequestUserAutoLogin

        $info = array(
            "fn"					=> "eWallet_RequestUserAutoLogin",
            "MerchantGUID"			=> $this->_MerchantGUID,
            "MerchantPassword"	    => $this->_MerchantPassword,
            "UserName"				=> (string)$username
        );

        $response = $this->makeJsonCall($info);
        if ( $response['response']['m_Code'] == 'NO_ERROR' || $response['response']['m_Code'] == '0' )
        {
            // TODO: we have a valid eWallet account maybe update the status or do something here in the future
            return $response['response']['ProcessorTransactionRefNumber'];
        }

        if ( $response['response']['m_Code'] == 'CUSTOMER_NOT_FOUND' )
        {
            // TODO: The account isn't created, so lets create the account on the fly
        }
        return false;
    }

    private function currencyCodes($code) {
        switch ( (string)$code ) {
            case "USD":
                $return = array(
                    'text'  =>  'United States Dollars',
                    'value' =>  'USD',
                );
                break;
            case "EUR":
                $return = array(
                    'text'  =>  'Euro',
                    'value' =>  'EUR',
                );
                break;
            case "GBP":
                $return = array(
                    'text'  =>  'Great Britain Pound',
                    'value' =>  'GBP',
                );
                break;
            case "AUD":
                $return = array(
                    'text'  =>  'Australian Dollars',
                    'value' =>  'AUD',
                );
                break;
            case "JPY":
                $return = array(
                    'text'  =>  'Japanese Yen',
                    'value' =>  'JPY',
                );
                break;
        }
    }
    private function statusCode($code)
    {
        switch ( (string)$code ) {
            case "0":
                $return = array(
                    'text'          =>  'NO_ERROR',
                    'value'         =>  '0',
                    'descriptiopn'  =>  'Success.',
                );
                break;
            case "-1":
                $return = array(
                    'text'          =>  'INTERNAL_ERROR',
                    'value'         =>  '-1',
                    'description'   =>  'Internal error. Can happen if something is not set up right or database error.',
                );
                break;
            case "-2":

                $return = array(
                    'text'          =>  'CUSTOMER_NOT_FOUND',
                    'value'         =>  '-2',
                    'description'   =>  'User not found.',
                );
                break;
            case "-3":
                $return = array(
                    'text'          =>  'GENERAL_ERROR',
                    'value'         =>  '-3',
                    'description'   =>  'Any other error, look at m_Text field for an error description.',
                );
                break;
            case "-4":
                $return = array(
                    'text'          =>  'INVALID_PASSWORD',
                    'value'         =>  '-4',
                    'description'   =>  'Invalid password.',
                );
                break;
            case "-5":
                $return = array(
                    'text'          =>  'ACCOUNT_NOT_FOUND',
                    'value'         =>  '-5',
                    'description'   =>  'Account not found.',
                );
                break;
            case "-6":
                $return = array(
                    'text'          =>  'NOT_AVAILABLE_FOR_THIS_TYPE',
                    'value'         =>  '-6',
                    'description'   =>  'Functionality is not available for this type of account.',
                );
                break;
            case "-7":
                $return = array(
                    'text'          =>  'INVALID_CLIENT_CREDENTIALS',
                    'value'         =>  '-7',
                    'description'   =>  'Invalid client credentials.',
                );
                break;
            case "-8":
                $return = array(
                    'text'          =>  'CUSTOMER_ALREADY_LOGGED_IN',
                    'value'         =>  '-8',
                    'description'   =>  'User already logged in.',
                );
                break;
            case "-9":
                $return = array(
                    'text'          =>  'METHOD_IS_AVAILABLE_BUT_OBSOLETE',
                    'value'         =>  '-9',
                    'description'   =>  'Method is declared unnecessary/obsolete. Please contact IPS.',
                );
                break;
            case "-10":
                $return = array(
                    'text'          =>  'USER_NOT_LOGGED_IN',
                    'value'         =>  '-10',
                    'description'   =>  'User is not logged in or timeout occurred.',
                );
                break;
            case "-11":
                $return = array(
                    'text'          =>  'PROCESSORID_NOT_FOUND',
                    'value'         =>  '-11',
                    'description'   =>  'Processor is not found. It means that there is a problem with configuration in merchants profile. Please contact IPS',
                );
                break;
            case "-12":
                $return = array(
                    'text'          =>  'USERNAME_EXISTS',
                    'value'         =>  '-12',
                    'description'   =>  'A user with this User Name already exists. It may happen during the registration.',
                );
                break;
            case "-13":
                $return = array(
                    'text'          =>  'EMAIL_EXISTS',
                    'value'         =>  '-13',
                    'description'   =>  'A user with this email already exists.',
                );
                break;
            case "-14":
                $return = array(
                    'text'          =>  'SSN_EXISTS',
                    'value'         =>  '-14',
                    'description'   =>  'A user with this SSN already exists.',
                );
                break;
            case "-15":
                $return = array(
                    'text'          =>  'INSUFFICIENT_FUNDS',
                    'value'         =>  '-15',
                    'description'   =>  'Insufficient funds.',
                );
                break;
            case "-16":
                $return = array(
                    'text'          =>  'NO_DEFAULT_CARD',
                    'value'         =>  '-16',
                    'description'   =>  'There is no default prepaid card set up for this user.',
                );
                break;
            case "-17":
                $return = array(
                    'text'          =>  'CARD_NOT_EXISTS',
                    'value'         =>  '-17',
                    'description'   =>  'Card does not exist.',
                );
                break;
            case "-18":
                $return = array(
                    'text'          =>  'CARD_ALREADY_ASSIGNED',
                    'value'         =>  '-18',
                    'description'   =>  'Card is already assigned to another user.',
                );
                break;
            case "-19":
                $return = array(
                    'text'          =>  'INVALID_PIN',
                    'value'         =>  '-19',
                    'description'   =>  'Invalid PIN.',
                );
                break;
            case "-20":
                $return = array(
                    'text'          =>  'PROCESSORID_FOR_CARD_NOT_FOUND',
                    'value'         =>  '-20',
                    'description'   =>  'Cannot find Processor ID for the card number.',
                );
                break;
            case "-21":
                $return = array(
                    'text'          =>  'ALIAS_ALREADY_EXISTS',
                    'value'         =>  '-21',
                    'description'   =>  'This alias/nick name already exists for this user. All aliases must be unnique per user.',
                );
                break;
            case "-22":
                $return = array(
                    'text'          =>  'ERROR_SAVING_CUSTOMER_DETAILS',
                    'value'         =>  '-22',
                    'description'   =>  'Error saving customer details.',
                );
                break;
            case "-23":
                $return = array(
                    'text'          =>  'DRIVERLICENSE_EXISTS',
                    'value'         =>  '-23',
                    'description'   =>  'Drivers license already exists. All DL must be unique per merchant.',
                );
                break;
            case "-24":
                $return = array(
                    'text'          =>  'PASSPORT_EXISTS',
                    'value'         =>  '-24',
                    'description'   =>  'Passport number already exists.',
                );
                break;
            case "-25":
                $return = array(
                    'text'          =>  'CARD_ALREADY_ASSIGNED_TO_YOU',
                    'value'         =>  '-25',
                    'description'   =>  'Prepaid card is already assigned to the user.',
                );
                break;
            case "-26":
                $return = array(
                    'text'          =>  'INVALID_CARD_NUMBER',
                    'value'         =>  '-26',
                    'description'   =>  'Invalid card number.',
                );
                break;
            case "-27":
                $return = array(
                    'text'          =>  'CARD_ALREADY_ACTIVATED',
                    'value'         =>  '-27',
                    'description'   =>  'Card is already activated.',
                );
                break;
            case "-28":
                $return = array(
                    'text'          =>  'CARD_STATUS_NOT_ISSUED_NOR_ACTIVE',
                    'value'         =>  '-28',
                    'description'   =>  'Card status is not issued or not active.',
                );
                break;
            case "-29":
                $return = array(
                    'text'          =>  'CARD_NOT_ACTIVE',
                    'value'         =>  '-29',
                    'description'   =>  'Card is not active.',
                );
                break;
            case "-30":
                $return = array(
                    'text'          =>  'PROCESSOR_ERROR',
                    'value'         =>  '-30',
                    'description'   =>  'Specific processors error.',
                );
                break;
            case "-31":
                $return = array(
                    'text'          =>  'NETWORK_FAILURE',
                    'value'         =>  '-31',
                    'description'   =>  'Network failure.',
                );
                break;
            case "-32":
                $return = array(
                    'text'          =>  'INVALID_ALIAS',
                    'value'         =>  '-32',
                    'description'   =>  'Invalid alias.',
                );
                break;
            case "-33":
                $return = array(
                    'text'          =>  'CANT_APPEND_TO_DESCRIPTION',
                    'value'         =>  '-33',
                    'description'   =>  'Cannot append text to transaction description.',
                );
                break;
            case "-34":
                $return = array(
                    'text'          =>  'CARD_ALREADY_ACTIVATED_NEED_PIN',
                    'value'         =>  '-34',
                    'description'   =>  'Card is already activated. PIN is required.',
                );
                break;
            case "-35":
                $return = array(
                    'text'          =>  'PAL_ALIAS_ALREADY_EXISTS',
                    'value'         =>  '-35',
                    'description'   =>  'Such pals alias already exists.',
                );
                break;
            case "-36":
                $return = array(
                    'text'          =>  'PAL_IS_ALREADY_ADDED',
                    'value'         =>  '-36',
                    'description'   =>  'You already added this pal.',
                );
                break;
            case "-37":
                $return = array(
                    'text'          =>  'PAL_INVALID_EMAIL',
                    'value'         =>  '-37',
                    'description'   =>  'Cannot find pal by such email.',
                );
                break;
            case "-38":
                $return = array(
                    'text'          =>  'TOTAL_AMOUNT_EXCEEDS_MAX_BALANCE',
                    'value'         =>  '-38',
                    'description'   =>  'Resulting total balance exceeds the max balance allowed on the card.',
                );
                break;
            case "-39":
                $return = array(
                    'text'          =>  'TOTAL_MONTH_TRANSFER_EXCEEDS_MAX_MONTHLY_TRANSFER',
                    'value'         =>  '-39',
                    'description'   =>  'Total monthly transfer exceeds the max monthly transfer allowed.',
                );
                break;
            case "-40":
                $return = array(
                    'text'          =>  'TOTAL_DAY_TRANSFER_EXCEEDS_MAX_DAY_TRANSFER',
                    'value'         =>  '-40',
                    'description'   =>  'Total daily transfer exceeds the max daily transfer allowed.',
                );
                break;
            case "-41":
                $return = array(
                    'text'          =>  'ALL_ACHS_FAILED',
                    'value'         =>  '-41',
                    'description'   =>  'All ACHs failed.',
                );
                break;
            case "-42":
                $return = array(
                    'text'          =>  'CANNOT_RETRIEVE_PIN',
                    'value'         =>  '-42',
                    'description'   =>  'Cannot retrieve PIN.',
                );
                break;
            case "-43":
                $return = array(
                    'text'          =>  'CANNOT_INSERT_BANK_ACCOUNT',
                    'value'         =>  '-43',
                    'description'   =>  'Cannot insert bank account.',
                );
                break;
            case "-44":
                $return = array(
                    'text'          =>  'CANNOT_TRANSFER_TO_BANK_ACCOUNT',
                    'value'         =>  '-44',
                    'description'   =>  'Cannot transfer to bank account.',
                );
                break;
            case "-45":
                $return = array(
                    'text'          =>  'INVALID_BANKID',
                    'value'         =>  '-45',
                    'description'   =>  'Invalid Bank ID.',
                );
                break;
            case "-46":
                $return = array(
                    'text'          =>  'INVALID_VIRTUAL_ACCOUNT_ID',
                    'value'         =>  '-46',
                    'description'   =>  'Invalid eWallet ID.',
                );
                break;
            case "-47":
                $return = array(
                    'text'          =>  'INVALID_CARDID',
                    'value'         =>  '-47',
                    'description'   =>  'Invalid Card ID.',
                );
                break;
            case "-48":
                $return = array(
                    'text'          =>  'INVALID_ACH_PROCESSOR',
                    'value'         =>  '-48',
                    'description'   =>  'There is no ACH Processor defined in the database.',
                );
                break;
            case "-49":
                $return = array(
                    'text'          =>  'MILITARY_ID_ALREADY_EXISTS',
                    'value'         =>  '-49',
                    'description'   =>  'Military id already exists.',
                );
                break;
            case "-50":
                $return = array(
                    'text'          =>  'INCOMPATIBLE_SOURCE_AND_DESTINATION_ACCOUNTS',
                    'value'         =>  '-50',
                    'description'   =>  'Incompatible source and destination accounts.',
                );
                break;
            case "-51":
                $return = array(
                    'text'          =>  'INVALID_PSEUDODDA_NUMBER',
                    'value'         =>  '-51',
                    'description'   =>  'The Pseudo DDA number and/or the routing number of the card are/is invalid.',
                );
                break;
            case "-52":
                $return = array(
                    'text'          =>  'MOBILE_PASSWORD_IS_NOT_SET',
                    'value'         =>  '-52',
                    'description'   =>  'Mobile password is not setup.',
                );
                break;
            case "-53":
                $return = array(
                    'text'          =>  'INVALID_MERCHANT',
                    'value'         =>  '-53',
                    'description'   =>  'Invalid merchant.',
                );
                break;
            case "-54":
                $return = array(
                    'text'          =>  'NOT_ACTIVE',
                    'value'         =>  '-54',
                    'description'   =>  'Not active.',
                );
                break;
            case "-55":
                $return = array(
                    'text'          =>  'NOT_PAID',
                    'value'         =>  '-55',
                    'description'   =>  'Not paid.',
                );
                break;
            case "-56":
                $return = array(
                    'text'          =>  'NO_RECORDS_FOUND',
                    'value'         =>  '-56',
                    'description'   =>  'No records found.',
                );
                break;
            case "-57":
                $return = array(
                    'text'          =>  'PENDING',
                    'value'         =>  '-57',
                    'description'   =>  'Transaction is Pending.',
                );
                break;
            case "-58":
                $return = array(
                    'text'          =>  'PROCESSING',
                    'value'         =>  '-58',
                    'description'   =>  'Transaction is Processing.',
                );
                break;
            case "-59":
                $return = array(
                    'text'          =>  'EMAIL_REQUIRED',
                    'value'         =>  '-59',
                    'description'   =>  'Email is required.',
                );
                break;
            case "-60":
                $return = array(
                    'text'          =>  'INVALID_COUNTRY_CODE',
                    'value'         =>  '-60',
                    'description'   =>  'Invalid Country Code',
                );
                break;
            case "-61":
                $return = array(
                    'text'          =>  'ACCESS_DENIED',
                    'value'         =>  '-61',
                    'description'   =>  'Access permission is required to use this function.',
                );
                break;
        }

        return $return;
    }
    private function makeJsonCall($info)
    {
        $response = null;
        $json_info = json_encode($info);

        $curl = cURL::jsonPost($this->_eWalletJsonAPIURL, $info);

        $log_msg = "iPayout.makeCall: json request : " . $json_info . " -- " . "iPayout.makeCall: json response: " . $curl->body;
        $this->write_debug($log_msg);
        return json_decode($curl->body, true);
    }
    private function returnResponse()
    {
        $arrayReturn = array(
            'eWallet_response'      =>  $this->_eWalletResponse,
            'response'              =>  $this->_friendlyResponse,
        );

        return $arrayReturn;
    }
    private function write_debug($msg)
    {
        Log::debug($msg);
    }
}