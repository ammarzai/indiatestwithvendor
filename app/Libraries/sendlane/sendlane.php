<?php
namespace App\Libraries\sendlane;
/**
 * Copyright (C) 2017, Innovations Technology Group, Inc.
 * Filename: sendlane.php
 */
use anlutro\cURL\Laravel\cURL;
use Illuminate\Support\Facades\Log;

class Sendlane
{
    // http://help.sendlane.com/knowledgebase/api-docs/
    private $_apiKey;
    private $_hashKey;
    private $_apiUrl;
    private $_leadTable;

    public function __construct($apiKey=null, $hashKey=null, $apiUrl=null)
    {
        if ( is_null($apiKey) === true ) {
            $this->_apiKey = env('SENDLANE_APIKEY');
        } else {
            $this->_apiKey = $apiKey;
        }

        if ( is_null($hashKey) === true ) {
            $this->_hashKey = env('SENDLANE_HASHKEY');
        } else {
            $this->_hashKey = $hashKey;
        }

        if ( is_null($apiUrl) === true ) {
            $this->_apiUrl = env('SENDLANE_APIURL');
        } else {
            $this->_apiUrl = $apiUrl;
        }

        $this->_leadTable = "users_leads";
    }
    public function addSubscriberToList($list_id, $lead_id, $extra_params=[])
    {
        $query = "SELECT * FROM " . $this->_leadTable . " WHERE id = '{$lead_id}'";
        $select = \DB::select($query);

        if ( is_null($select) === false )
        {
            $row = $select[0];
        } else {
            return false;
        }

        $api_call = '/api/v1/list-subscriber-add';
        $params = array(
            'email'                 =>  $row->email,
            'list_id'               =>  $list_id,
            "sponsor_id_{$list_id}" =>  $row->users_id,
        );

        if ( count($extra_params) > 0 ) {
            $params = array_merge($params, $extra_params);
        }

        if ( property_exists($row, 'first_name') === true ) { $params["first_name"] = $row->first_name; } else { $params['first_name'] = 'Friend'; }
        if ( property_exists($row, 'phone') === true ) { $params["phone"] = $row->phone; }

        $results = $this->execute($api_call, $params);
        $results_return = json_decode(json_encode($results), true);

        return $results_return;
    }
    public function addSubscriberToListWithInfo($list_id, $firstname, $email, $phone)
    {
        $api_call = '/api/v1/list-subscriber-add';
        $params = array(
            'email'             =>  $email,
            'list_id'           =>  $list_id,
            'first_name'        =>  $firstname,
            'phone_number'      =>  $phone,
        );
        $results = $this->execute($api_call, $params);
        $results_return = json_decode(json_encode($results), true);

        return $results_return;
    }
    public function delSubscriberFromList($list_id, $lead_id)
    {
        $query = "SELECT * FROM " . $this->_leadTable . " WHERE id = '{$lead_id}'";
        $select = \DB::select($query);
        if ( is_null($select) === false )
        {
            $row = $select[0];
        } else {
            return false;
        }

        $api_call = '/api/v1/subscribers-delete';
        $params = array(
            'email'         =>  $row->email,
            'list_id'       =>  $list_id,
        );

        $results = $this->execute($api_call, $params);
        $results_return = json_decode(json_encode($results), true);

        return $results_return;

    }
    public function lists($list_id=null)
    {
        if ( is_null($list_id) === true )
        {
            return $this->execute('/api/v1/lists', array('start' => 0, 'limit' => 99));
        } else {
            return $this->execute('/api/v1/lists', array('start' => 0, 'limit' => 99, 'list_id' => $list_id));
        }
    }
    public function verifyApiInfo()
    {
        $results = $this->lists();
        if ( is_array($results) === true ) {
            $key_exists = array_key_exists('error', $results);
        } else {
            $key_exists = true; // this is true because error would have been found above.. not sure if sendlane changed their api response for this lists call...
        }
        if ( $key_exists === false ) { return true; }
        if ( $key_exists === true ) { return false; }

    }
    private function execute($api, $post_params=null)
    {
        if ( is_null($post_params) === true )
        {
            $post_params = array();
        }

        $post_params['api'] = $this->_apiKey;
        $post_params['hash'] = $this->_hashKey;

        $curl = cURL::post("http://" . $this->_apiUrl . $api, $post_params);

        $post_email = '';
        $post_list_id = '';

        if ( array_key_exists('email', $post_params) === true ) {
            $post_email = $post_params['email'];
        }
        if ( array_key_exists('list_id', $post_params) === true ) {
            $post_list_id = $post_params['list_id'];
        }
        Log::debug("Sendlane API Key: " . $post_params['api'] . " Email: " . $post_email . " List ID: " . $post_list_id . " Response: " . $curl->body);
        return json_decode($curl->body, true);

        //$connect = new Curl;
        //$response = $connect->post('https://' . $this->_apiUrl . $api, $post_params);
        //$show_response = $connect->response;
        //$json_response = json_encode($show_response);
        //return $show_response;
    }
}