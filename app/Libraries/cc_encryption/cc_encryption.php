<?php
namespace App\Libraries\cc_encryption;

use Illuminate\Support\Facades\Crypt;

/**
 * Created by PhpStorm.
 * User: jasoncullins
 * Date: 2/4/14
 * Time: 5:24 PM
 */
class cc_encryption
{
    const CIPHER = MCRYPT_RIJNDAEL_128;
    const MODE = MCRYPT_MODE_CBC;


    private $salt;

    public function __construct($salt = 'fcn985yudb0q2783')
    {
        $this->salt = $salt;
    }

    public function encrypt_text($text)
    {
        $secure_key = hash('sha256', $this->salt, true);

        return base64_encode(mcrypt_encrypt(self::CIPHER, $secure_key, $text, MCRYPT_MODE_ECB));
    }

    public function decrypt_text($text)
    {
        $secure_key = hash('sha256', $this->salt, true);

        return trim(mcrypt_decrypt(self::CIPHER, $secure_key, base64_decode($text), MCRYPT_MODE_ECB));
    }

    public function encrypt_cc_mcrypt($plaintext)
    {
        $ivSize = mcrypt_get_iv_size(self::CIPHER, self::MODE); //Gets the size of the IV belonging to a specific cipher/mode combination.
		$iv = mcrypt_create_iv($ivSize, MCRYPT_DEV_RANDOM); //Creates an initialization vector (IV) from a random source.
		$ciphertext = mcrypt_encrypt(self::CIPHER, $this->salt, $plaintext, self::MODE, $iv); //Encrypts the data and returns it.
		return base64_encode($iv . $ciphertext); //Encode Base 64
    }
    public function encrypt_cc($plaintext)
    {
        return encrypt($plaintext);
    }

    public function decrypt_cc_mcrypt($ciphertext)
    {
        $ciphertext = base64_decode($ciphertext); //Decode Base 64
        $ivSize = mcrypt_get_iv_size(self::CIPHER, self::MODE); //Gets the size of the IV belonging to a specific cipher/mode combination.
        if ( strlen($ciphertext) < $ivSize ) {
            throw new Exception('Missing initialization vector');
        }

        $iv = substr($ciphertext, 0, $ivSize);
        $ciphertext = substr($ciphertext, $ivSize);
        $plaintext = mcrypt_decrypt(self::CIPHER, $this->salt, $ciphertext, self::MODE, $iv); //Decrypts the data and returns it.
        return rtrim($plaintext, "\0");
    }
    public function decrypt_cc($ciphertext)
    {
        return decrypt($ciphertext);
    }
}

?>
