

@extends('templates.admin.layout')

@section('content')
<div class="">

    <div class="row">

        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_title">
                    <h2>Customer Orders</h2>
                    <div class="clearfix"></div>
                </div>
                <div class="x_content">
                    <table id="datatable-buttons" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Order #</th>
                                <th>Product</th>
                                <th>Image</th>
                                <th>Quantity</th>
                                <th>Unit Price</th>
                                <th>Total</th>
                                
                            </tr>
                        </thead>
                        
                        <tbody>
                            @if(count($orders))
                            @php $total=0; @endphp
                            @foreach($orders[0]->orderdetail as $row)
                            @php $total=$total+$row->sub_total; @endphp
                            <tr>
                                <td>{{$row->order_number_detail}}</td>
                                <td>{{$row->orderdetailproduct->product_name}}</td>
                                <td><img src="{{asset('uploads')}}/{{$row->orderdetailproduct->product_image}}" class="img-thumbnail img-responsive" style="width: 100px;"></td>
                                <td>{{$row->quantity}}</td>
                                <td>{{$row->price}}</td>
                                <td>{{$row->sub_total}}</td>
                                
                            </tr>
                            @endforeach
                            <tr>
                                <td colspan="5" style="text-align: right;"><b>Total:{{$total}}</b></td>
                            </tr>
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@stop