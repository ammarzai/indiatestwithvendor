<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAutoresponderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('autoresponder_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('app_id');
            $table->unsignedInteger('user_autoresponder_id');
			$table->foreign('user_autoresponder_id')->references('id')->on('user_autoresponder');
            $table->text('accessToken');
            $table->text('accessTokenSecret');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('autoresponder_details');
    }
}
