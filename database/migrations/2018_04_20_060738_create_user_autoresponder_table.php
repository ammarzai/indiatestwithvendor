<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserAutoresponderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_autoresponder', function (Blueprint $table) {
            $table->increments('id');
            /*$table->integer('users_id');
            $table->integer('autoresponder_id');*/
            
            $table->unsignedInteger('user_id');
			$table->foreign('user_id')->references('id')->on('users');
			$table->unsignedInteger('autoresponder_id');
			$table->foreign('autoresponder_id')->references('id')->on('autoresponder');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_autoresponder');
    }
}
